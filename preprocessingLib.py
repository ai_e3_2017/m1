#import nltk
#nltk.download('wordnet')
from nltk import pos_tag, RegexpParser, ne_chunk
from nltk.tokenize import word_tokenize, sent_tokenize
from nltk.stem import PorterStemmer
from nltk.corpus import wordnet, stopwords
from nltk.stem import WordNetLemmatizer
import string
"""
POS tag list:

CC	coordinating conjunction
CD	cardinal digit
DT	determiner
EX	existential there (like: "there is" ... think of it like "there exists")
FW	foreign word
IN	preposition/subordinating conjunction
JJ	adjective	'big'
JJR	adjective, comparative	'bigger'
JJS	adjective, superlative	'biggest'
LS	list marker	1)
MD	modal	could, will
NN	noun, singular 'desk'
NNS	noun plural	'desks'
NNP	proper noun, singular	'Harrison'
NNPS	proper noun, plural	'Americans'
PDT	predeterminer	'all the kids'
POS	possessive ending	parent's
PRP	personal pronoun	I, he, she
PRP$	possessive pronoun	my, his, hers
RB	adverb	very, silently,
RBR	adverb, comparative	better
RBS	adverb, superlative	best
RP	particle	give up
TO	to	go 'to' the store.
UH	interjection	errrrrrrrm
VB	verb, base form	take
VBD	verb, past tense	took
VBG	verb, gerund/present participle	taking
VBN	verb, past participle	taken
VBP	verb, sing. present, non-3d	take
VBZ	verb, 3rd person sing. present	takes
WDT	wh-determiner	which
WP	wh-pronoun	who, what
WP$	possessive wh-pronoun	whose
WRB	wh-abverb	where, when
"""

def fReader(fname):
    with open(fname, encoding="UTF-8") as myfile:
        data = myfile.read().replace('<', 'less than').replace('>', 'more than').replace('&', 'and')
 #       data = myfile.read()

    return data


def separate_by_paragraphs(text):
    '''This function will receive as input raw text\nand give as output a list of all paragraphs found.'''
    return text.split("\n")

def tokenize_paragraph(paragraph):
    '''This function will receive as input a paragraph\nand give as output a list of all sentences found.'''
    return sent_tokenize(paragraph)

def tokenize_sentence(sentence):
    '''This function will receive as input a sentence\nand give as output a list of all tokens found.'''
    return word_tokenize(sentence)

def stopWords(test):
    '''stopWords function receives a list as input and returns a list without punctuation marks and stopWords'''

    stopWords= set (stopwords.words("english"))
    #print(stopWords)

    newTest= ["".join( j for j in i if j not in string.punctuation) for i in  test]

    newTest = [s for s in newTest if s]

    finalScript=[]

    for word in newTest:
        if word not in stopWords:
            finalScript.append(word)


    return finalScript

def punctMarksOnly(test):
    ''' punctMarksOnly function receives a list as input and returns a list without punctuation marks '''

    newTest = ["".join(j for j in i if j not in string.punctuation) for i in test]

    newTest = [s for s in newTest if s]

    return newTest

def stopWordsOnly(test):
    ''' stopWordsOnly function receives a list as input and returns a list without stopWords'''

    stopWords = set(stopwords.words("english"))
    # print(stopWords)

    finalScript = []

    for word in test:
        if word not in stopWords:
            finalScript.append(word)

    return finalScript


def punctMarks(test):


    'This function returns list with all the punctuation marks displayed in the text'

    newTest = ["".join(j for j in i if j  in string.punctuation) for i in test]

    newTest = [s for s in newTest if s]

    return newTest

def stopWordsList(test):


    'This function returns a list with all the stopWords that appear in the text'
    stopWords = set(stopwords.words("english"))
    # print(stopWords)

    finalScript = []

    for word in test:
        if word.lower()  in stopWords:
            finalScript.append(word)


    return finalScript


def chunking(tokenized):
    """
    Chunking in Natural Language Processing (NLP) is the process by which we group various words
    together by their part of speech tags.
    One of the most popular uses of this is to group things by what are called "noun phrases."
    We do this to find the main subjects and descriptive words around them, but chunking can be
    used for any combination of parts of speech.
    """

    try:
        for i in tokenized:
            words = word_tokenize(i)
            tagged = pos_tag(words)

            chunkGram = r"""
                Chunk:  {(<DT>|<JJ>|<NN>)+}  
                        {<NNP>+} 
                        {<NN.*|JJ>*<NN.*>}
                        {<NN>+}
            """
            chunkParser = RegexpParser(chunkGram)
            chunked = chunkParser.parse(tagged)
            return chunked

    except Exception as e:
        print(str(e))

def posTagger(tokenized):
    '''The posTagger function take as paramerer a list of tokenized words and returns a
    list consisting of pairs word - part of speech tag'''
    try:
        for i in tokenized:
            words = tokenize_sentence(i)
            tagged = pos_tag(words)
        return tagged

    except Exception as e:
        print(str(e))

def posTaggerWord(sentence):
    w = word_tokenize(sentence)
    p = pos_tag(w)
    return p

def process_content(tokenized):
	'''The process_content function takes as a parameter a list of tokenized words and recognises\n
	either a general named entity, or it can recognize names, locations, monetary amounts, dates...'''

	try:
		for i in tokenized:
			words = word_tokenize(i)
			tagged = pos_tag(words)

			namedEnt = ne_chunk(tagged)

			namedEnt.draw()

	except Exception as e:
		print(str(e))

def _stemm(text):
    '''This function will receive as input raw text\nand give as output a list of unique stemms found.'''
    ps = PorterStemmer()
    words = tokenize_sentence(text)
    result = set()
    for w in words:
        result.add(ps.stem(w))
    return list(result)

def get_wordnet_pos(treebank_tag):
    """
    This function translates nltk tags into lemmatizing tags needed into _lemmatize() function
    :param treebank_tag: NLTK tag
    :return: lemmatizing tag
    """

    if treebank_tag.startswith('J'): #adjective
        return "a"
    elif treebank_tag.startswith('V'):  #verb
        return "v"
    elif treebank_tag.startswith('N'): #noun
        return "n"
    elif treebank_tag.startswith('R'): #adverb
        return "r"
    else:
        return ""

def _lemmatize(word, token):
    '''This function will receive as input a list of (word - part of speech) pairs and give as output a list of lemmatized words.'''
    wnl = WordNetLemmatizer()
    result = set()
    lemma_pos = get_wordnet_pos(token)
    if not lemma_pos.strip():
        result.add(wnl.lemmatize(word.lower()))
    else:
        result.add(wnl.lemmatize(word.lower(), pos=lemma_pos))

    return list(result)

def has_hypernym(word1, word2):
    '''
    This function will receive as input two words\na nd query wordnet to find whether the second word\n is a
    hypernym for the first word:\n true : if they are\n false: otherwise.
    '''

    def extract_word_from_synset(synset):
        '''
        This function will receive as input a synset and give as output the word of that synset
        '''
        return synset.name().split(".")[0]

    synArray = wordnet.synsets(word1)
    main_synset = synArray[0]

    for hypernym in main_synset.hypernyms():
        if extract_word_from_synset(hypernym) == word2:
            return True

    return False

def get_hypernym_relations(words):
    '''This function will receive as input a list of words\nand give as output a dictionary.\nEach key represents a
    word.\nEach value represents a list of higher concepts for\nthe corresponding key\n(these concepts can also be
    found in the input list).'''
    relations = dict()
    for i in range(len(words)):
        for j in range(len(words)):
            if i is not j:
                if has_hypernym(words[i], words[j]):
                    if words[i] in relations:
                        relations[words[i]].append(words[j])
                    else:
                        relations[words[i]] = [words[j]]
    return relations