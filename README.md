# README #

### What is this repository for? ###

This Repo represent a sub-module, part of the English Group Module for Artificial Intelligente semestrial project, which is "The Acquisition of an Ontholoy Based on Text Documentation".
The sub-module's main goal is to process raw text (tokenise, POS, NP, VP).
Input: raw text
Output: XML format with the annotated text that includes as much information as possible regarding the words in the context

### Team and individual activity ###

The team consists of 9 members.
We use different work methodologies in order to maximize the efficiency. (eg. Kanban, Scrum)

The tasks can be seen at the following link: 
https://trello.com/invite/b/nq7chrgC/d91824198268ac3b013a672a9592f896/fii-e-aiproject-m1

The individual activity can be checked at: 
https://docs.google.com/spreadsheets/d/1SAUZ9KYzMasvV56ClMoAHmD9sf8IRxA2cMTAT8DXHOU/edit?usp=sharing

### Who do I talk to? ###

The team leader, of course.
Razvan Nica,
e-mail: razvan.nica30@gmail.com |
facebook: https://www.facebook.com/nica.razvan.33 |
skype: razvan.nica30
